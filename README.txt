                   _ _ _
                 /__/__/__/\
                /__/__/__/\/\
               /__/__/__/\/\/\
           Solver De Rubik's cube
               \__\__\__\/\/\/
                \__\__\__\/\/       Created by: carmesaure@eisti.eu;
                 \__\__\__\/                    gadeathimo@eisti.eu;
                                                pointereau@eisti.eu;
                                                alexisslot@eisti.eu;
                                    Version 1.0


###############################
# Standards de représentation #
###############################

Tout au long de ce projet, le rubik's cube sera représenté avec:
 -Face Du Bas: Blanc
 -Face Du Dessus: Jaune
 -Face De Derriere: Vert
 -Face De Devant: Bleu
 -Face De Droite: Rouge
 -Face De Gauche: Orange

##########################################
# Normes de représentations du mouvement #
##########################################

Voici les differentes lettres qui apparaitrons dans le programme suivi de leurs traductions:

R : Rotation de la face droite de 90° dans le sens de l'aiguille d'une montre
R': Rotation de la face droite de 90° dans le sens inverse de l'aiguille d'une montre

L : Rotation de la face gauche de 90° dans le sens de l'aiguille d'une montre
L': Rotation de la face gauche de 90° dans le sens inverse de l'aiguille d'une montre

B : Rotation de la face bas de 90° dans le sens de l'aiguille d'une montre
B': Rotation de la face bas de 90° dans le sens inverse de l'aiguille d'une montre

F : Rotation de la face face a nous de 90° dans le sens de l'aiguille d'une montre
F': Rotation de la face face a nous de 90° dans le sens inverse de l'aiguille d'une montre

U : Rotation de la face d'en haut de 90° dans le sens de l'aiguille d'une montre
U': Rotation de la face d'en haut de 90° dans le sens inverse de l'aiguille d'une montre

D : Rotation de la face d'en bas de 90° dans le sens de l'aiguille d'une montre
D': Rotation de la face d'en bas de 90° dans le sens inverse de l'aiguille d'une montre


##############################
# Utilisation du programme   #
##############################
-Sur linux:
Pour utiliser le programme, ouvrez un terminal et déplacez vous jusqu'au programme (cd ~/<nom du  dossier>). Ensuite executez le programme (./<nom du programme>).
Une fois a l'interieur, veuillez suivre les indications ecrites pour rentrez la configuration initiale de votre cube.
Si vous avez correctement suivis les étapes demandés le programme devrait vous afficher les mouvements necessaires (avec les normes de représentation décrites au dessus) afin de pouvoir resoudre votre rubiks cube!


################
# Code Couleur #
################

Pour le code nous utilisons un code couleur particulier, c'est a dire un chiffre est associé à une couleur:
Blanc:1
Jaune:2
Bleu:3
Vert:4
Orange:5
Rouge:6
