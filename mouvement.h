/*!
  \file mouvement.h
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Mon Dec  9 20:00:24 2019
  \brief fichier entete mouvements

  Contient les fonctions permettant d'effectuer de smouvements sur les cubes (transformations)
*/

#ifndef MOUVEMENT_H
#define MOUVEMENT_H

/* inclusion des fichiers entete */
#include "structures.h"

//



/*!
  \fn triple matProd (triple vec, int tt_mat[3][3])
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Mon Dec  9 20:03:11 2019
  \brief Effectue le produit matriciel d'un vecteur avec une matrice carree de taille 3
  \param vec vecteur à transformer
  \param tt_mat matrice 3x3 à appliquer
  \return vecteur transformé
*/
triple matProd (triple vec, int tt_mat[N][N]);


/*!
  \fn void appliquerMouv (cubie Cube[TAILLE_CUBE], int tt_mat[N][N], triple triple_face)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Mon Dec  9 20:08:27 2019
  \brief Procedure pour appliquer un mouvement sur le cube
  \param Cube tableau contenant les cubie du cube considéré
  \param tt_mat Matrice du mouvment à appliquer
  \param triple_face face où appliquer le mouvement


  La procédure applique la matrice de mouvement à tous les cubies de la face concernée, et effectue les changements d'orientation. Le paramètre triple face est les coordonnées du centre de la face.
*/
void appliquerMouv (cubie Cube[TAILLE_CUBE], int tt_mat[N][N], triple triple_face);


/*!
  \fn void afficherCube (cubie Cube[TAILLE_CUBE])
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Mon Dec  9 20:14:18 2019
  \brief Affiche l'etat du cube dans le terminal
  \param Cube le cube à afficher

  >>> peut-etre à découper en plusieurs procedures et fonctions
*/
void afficherCube (cubie Cube[TAILLE_CUBE]);


/*!
  \fn void void appliquerOrient (cubie c_cubie, triple vec_position)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sun Dec 15 15:43:08 2019
  \brief applique le changement d'orientation asocié a un mouvement
  \param c_cubie adresse cubie a modifier
  \param int_indConst indice de la coordonnee constante lors du mouvement (axe perpendiculaire à la face)
*/
void appliquerOrient (cubie* c_cubie, int int_indConst);


/*!
  \fn void mouvFace(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, triple triple_face, int int_sens)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sun Jan 19 16:44:49 2020
  \brief Effectue une rotation d'une face dans un sens donné
  \param Cube cube utilise
  \param m_Matrices matrices de rotation
  \param triple_face centre de la face concernee
  \param int_sens sens de rotation

  Effectue une rotation de la face de centre triple_face dans le sens int_sens (1 pour sens horaire, -1 sens anti-horaire).

*/
void mouvFace(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, triple triple_face, int int_sens);






#endif
