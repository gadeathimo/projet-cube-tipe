#include<stdlib.h>
#include<stdio.h>
#include "resolutionCroix.h"
#include "mouvement.h"
#include "structures.h"



/*
A partir d'une croix partiellement triée, la procédure inverses les deux cubies mal placées s'ils le sont, puis pivote la face blanche pour aligner correctement la croix
*/
void swapCroix(cubie Cube[TAILLE_CUBE], Matrices m_Matrices)
{
  triple fBlanche;
  fBlanche= initTriple(0, -1, 0);

  /* Permutation de la croix dans la bonne position */

  if (!estSuivante(Cube[indexActuel(Cube, initTriple(0, -1, 1))].posFinale, Cube[indexActuel(Cube, initTriple(-1, -1, 0))].posFinale, 1)) { /* si la croix n'est pas bien permutée */
    if (!compareTriple(Cube[indexActuel(Cube, initTriple(0, -1, 1))].posFinale, initTriple(0, -1, 1))) { /* si le cubie de devant n'est pas à sa place */
      inversionCroix(Cube, m_Matrices, initTriple(0, -1, 1));
    }
    else {
      inversionCroix(Cube, m_Matrices, initTriple(-1, -1, 0));
    }

  }

  /* Rotation de la face blanche pour alignement de la croix*/
  if (compareTriple(Cube[indexActuel(Cube, initTriple(0, -1, 1))].posFinale, initTriple(-1, -1, 0))) {
    mouvFace(Cube, m_Matrices, fBlanche, -1);
    //printf("%d,%d,%d", Cube[indexActuel(Cube, initTriple(0, -1, 1))].posFinale[0], Cube[indexActuel(Cube, initTriple(0, -1, 1))].posFinale[1], Cube[indexActuel(Cube, initTriple(0, -1, 1))].posFinale[2]);
  }
  else if (compareTriple(Cube[indexActuel(Cube, initTriple(0, -1, 1))].posFinale, initTriple(1, -1, 0))) {
    mouvFace(Cube, m_Matrices, fBlanche, 1);
  }
  else if (compareTriple(Cube[indexActuel(Cube, initTriple(0, -1, 1))].posFinale, initTriple(0, -1, -1))) {
    mouvFace(Cube, m_Matrices, fBlanche, 1);
    mouvFace(Cube, m_Matrices, fBlanche, 1);
  }

  //
  free(fBlanche);
}


/*
permet d'inverser l'arrete t_pos et son opposé pour permuter correctement la croix
*/
void inversionCroix(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, triple t_pos)
{
  triple t_face;
  triple fBlanche;
  fBlanche= initTriple(0, -1, 0);

  /* determination de la face laterale contenant l'arete concernee */
  if (t_pos[0] == 0) {
    t_face = initTriple(0, 0, t_pos[2]);
  }
  else {
    t_face = initTriple(t_pos[0], 0, 0);
  }

  /* permutation des deux aretes */
  mouvFace(Cube, m_Matrices, t_face, 1);

  mouvFace(Cube, m_Matrices, fBlanche, 1);
  mouvFace(Cube, m_Matrices, fBlanche, 1);

  mouvFace(Cube, m_Matrices, t_face, -1);

  mouvFace(Cube, m_Matrices, fBlanche, 1);
  mouvFace(Cube, m_Matrices, fBlanche, 1);

  mouvFace(Cube, m_Matrices, t_face, 1);

  //
  free(t_face);
  free(fBlanche);
  free(t_pos);
}


/*
  En cas de rotation U ou D, renvoi vrai si la position2 sera la postion de l'arete actuellement en position1 apres le mouvement :  (U / D') pour le sens 1 et (U' / D) pour le sens -1
*/
int estSuivante(triple t_pos1, triple t_pos2, int int_sens)
{
  int int_retour;

  int_retour = (((((t_pos1[0] == 0) && ((t_pos1[2]) == (-t_pos2[0])))  ||  ((t_pos1[0] != 0) && ((t_pos1[0]) == (t_pos2[2])))) && (int_sens == 1))   ||
                ((((t_pos1[0] == 0) && ((t_pos1[2]) == (t_pos2[0])))  ||  ((t_pos1[0] != 0) && ((t_pos1[0]) == (-t_pos2[2])))) && (int_sens == -1)));

  return int_retour;
}


/*
Renvoit l'indice dans le Cube du cubie de position actuelle t_pos
*/
int indexActuel(cubie Cube[TAILLE_CUBE], triple t_pos)
{
  int i;

  i = 0;

  /* recherche l'indice en parcourant le tableau */
  while (!compareTriple(Cube[i].posActuelle, t_pos)) {
    i++;
  }

  return i;
  free(t_pos);
}


/*
Renvoit l'indice dans le Cube du cubie de position finale t_pos
*/
int indexFinal(cubie Cube[TAILLE_CUBE], triple t_pos)
{
  int i;

  i = 0;

  /* recherche l'indice en parcourant le tableau */
  while (!compareTriple(Cube[i].posFinale, t_pos)) {
    i++;
  }

  return i;
  free(t_pos);
}



/*
permet de retourner une arete de la croix
*/
void retourneAreteCroix(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index)
{
  triple t_face, fBlanche, fcote;
  triple t_pos;
  t_pos = Cube[int_index].posActuelle;
  fBlanche = initTriple(0, -1, 0);

  /* determine face laterale actuelle */
  if (t_pos[2] == 0) {
    t_face = initTriple(t_pos[0], 0, 0);
    fcote = initTriple(0, 0, -t_pos[0]);
  }
  else {
    t_face = initTriple(0, 0, t_pos[2]);
    fcote = initTriple(t_pos[2], 0, 0);
  }

  /*retourne l'arete*/
  mouvFace(Cube, m_Matrices, t_face, -1);
  mouvFace(Cube, m_Matrices, fBlanche, 1);
  mouvFace(Cube, m_Matrices, fcote, -1);
  mouvFace(Cube, m_Matrices, fBlanche, -1);

  /* libere la memoire */
  free(t_face);
  free(fBlanche);
  free(fcote);
  free(t_pos);
}


/*
Fait descendre une arete de la couronne vers la croix
*/
void moveAreteCouronne(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, int int_dest)
{
  int int_sens;
  triple t_face;

  /* determine la face à pivoter */
  if (Cube[int_index].int_orient == 0) {
    t_face = initTriple((Cube[int_index]).posActuelle[0], 0, 0);
    int_sens = - ((Cube[int_index].posActuelle[2]) * (Cube[int_index].posActuelle[0]));
  }
  else {
    t_face = initTriple(0, 0, (Cube[int_index]).posActuelle[2]);
    int_sens = (Cube[int_index].posActuelle[2]) * (Cube[int_index].posActuelle[0]);
  }

  /* replace la case de destination */
  replaceAreteLateral(Cube, m_Matrices, int_dest, t_face);

  /* replace l'arete vers la destination */
  mouvFace(Cube, m_Matrices, t_face, int_sens);

  free(t_face);
}


/*
  Replace une arete de la face blanche ou jaune au meme niveau qu'une des faces latérales en faisant une rotation de la face blanche ou jaune
*/
void replaceAreteLateral(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, triple t_face)
{
  triple faceSup;
  faceSup = initTriple(0, Cube[int_index].posActuelle[1], 0);

  if (estSuivante(Cube[int_index].posActuelle, initTriple(t_face[0], faceSup[1], t_face[2]), 1)) {
    mouvFace(Cube, m_Matrices, faceSup, Cube[int_index].posActuelle[1]);
  }
  else if (estSuivante(Cube[int_index].posActuelle, initTriple(t_face[0], faceSup[1], t_face[2]), -1)) {
    mouvFace(Cube, m_Matrices, faceSup, - Cube[int_index].posActuelle[1]);
  }
  else if ((Cube[int_index].posActuelle[0] == (- t_face[0])) && (Cube[int_index].posActuelle[2] == (- t_face[2]))){
    mouvFace(Cube, m_Matrices, faceSup, 1);
    mouvFace(Cube, m_Matrices, faceSup, 1);
  }

  free(faceSup);
}


/*
Fait descendre une arete de la face du haut vers la croix
*/
void moveAreteHaut(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, int int_dest)
{
  triple t_face, fBlanche, fcote;
  fBlanche = initTriple(0, -1, 0);

  if (Cube[int_index].posActuelle[0] == 0) {
    t_face = initTriple(0, 0, Cube[int_index].posActuelle[2]);
    fcote = initTriple(Cube[int_index].posActuelle[2], 0, 0);
  }
  else {
    t_face = initTriple(Cube[int_index].posActuelle[0], 0, 0);
    fcote = initTriple(0, 0, - Cube[int_index].posActuelle[0]);
  }

  /* replace l'arete sur la face blanche */
  if (Cube[int_index].int_orient == 0) {
    replaceAreteLateral(Cube, m_Matrices, int_dest, t_face);
    mouvFace(Cube, m_Matrices, t_face, 1);
    mouvFace(Cube, m_Matrices, t_face, 1);
  }
  else {
    replaceAreteLateral(Cube, m_Matrices, int_dest, fcote);
    mouvFace(Cube, m_Matrices, t_face, 1);
    mouvFace(Cube, m_Matrices, fcote, -1);
    mouvFace(Cube, m_Matrices, t_face, -1);
  }

  free(t_face);
  free(fcote);
  free(fBlanche);
}


/*
permet de placer les elements de la croix opposes deux a deux
*/
void faireCroix(cubie Cube[TAILLE_CUBE], Matrices m_Matrices)
{
  int i;
  int int_dest;
  int tint_aretes[4];
  /* Les 4 aretes a placer */
  tint_aretes[0] = indexFinal(Cube, initTriple(1, -1, 0));
  tint_aretes[1] = indexFinal(Cube, initTriple(-1, -1, 0));
  tint_aretes[2] = indexFinal(Cube, initTriple(0, -1, 1));
  tint_aretes[3] = indexFinal(Cube, initTriple(0, -1, -1));

  for (i = 0 ; i < 4 ; i++) {
    if ((i != 1) && (Cube[tint_aretes[i]].posActuelle[1] != -1)) { /* Si l'arete n'est pas sur la face blanche */
      int_dest = selectDestCroix(Cube);

      /* replace l'arete en fonction de sa position */
      if (Cube[tint_aretes[i]].posActuelle[1] == 0) {
        moveAreteCouronne(Cube, m_Matrices, tint_aretes[i], int_dest);
      }
      else {
        moveAreteHaut(Cube, m_Matrices, tint_aretes[i], int_dest);
      }
    }
    else if ((i != 1) && (Cube[tint_aretes[i]].posActuelle[1] == -1) && (Cube[tint_aretes[i]].int_orient == 1)) { /* Si l'arete est deja sur la face blanche, mais mal orientée */
      retourneAreteCroix(Cube, m_Matrices, tint_aretes[i]); /* retourne l'arete */
    }
    else if (i == 1) {                /* cas special pour placer la deuxieme arete */
      int_dest = indexActuel(Cube, initTriple(- Cube[tint_aretes[0]].posActuelle[0], -1, - Cube[tint_aretes[0]].posActuelle[2])); /* selectionne l'arete opposee a la premiere qui fut placee */

      /* cas si elle est deja sur la croix */
      if ((Cube[tint_aretes[i]].posActuelle[1] == -1) && (int_dest == tint_aretes[i]) && (Cube[tint_aretes[i]].int_orient == 1)) { /* si elle est bien placée, mais retournée */
        retourneAreteCroix(Cube, m_Matrices, tint_aretes[i]);
      }
      else if ((Cube[tint_aretes[i]].posActuelle[1] == -1) && (int_dest != tint_aretes[i])) { /* si elle est mal placée, mais deja sur la croix */
        mouvFace(Cube, m_Matrices, initTriple(Cube[tint_aretes[i]].posActuelle[0], 0, Cube[tint_aretes[i]].posActuelle[2]), 1); /* deplace l'arete sur la couronne */
      }

      /* replace l'arete en fonction de sa position */
      if (Cube[tint_aretes[i]].posActuelle[1] == 1) {
        moveAreteHaut(Cube, m_Matrices, tint_aretes[i], int_dest);
      }
      else if (Cube[tint_aretes[i]].posActuelle[1] == 0) {
        moveAreteCouronne(Cube, m_Matrices, tint_aretes[i], int_dest);
      }
    }
  }
}


/*
Selectionne la case où placer une arete pour la croix. Si aucune n'est disponible, la fonction renvoie -1
*/
int selectDestCroix(cubie Cube[TAILLE_CUBE])
{
  int i, j;
  int int_retour;

  i = 0;
  j = 1;
  int_retour = -1;

  /* teste les 4 positions de la croix blanche */
  while ((i < 4) && (int_retour == -1)) {
    /* si la case n'est pas occupée par une arete blanche */
    if ((i < 2) && ((Cube[indexActuel(Cube, initTriple(j, -1, 0))].posFinale[1]) != -1)) {
      int_retour = indexActuel(Cube, initTriple(j, -1, 0)); /* alors elle sera la destination */
    }
    else if ((i >= 2) && ((Cube[indexActuel(Cube, initTriple(0, -1, j))].posFinale[1]) != -1)) {
      int_retour = indexActuel(Cube, initTriple(0, -1, j));
    }

    i++;
    j *= -1;
  }

  return int_retour;
}
