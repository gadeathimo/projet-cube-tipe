#ifndef RESOLUTION_H
#define RESOLUTION_H

#include<stdlib.h>
#include<stdio.h>
#include "mouvement.h"
#include "structures.h"


void croixBlanche();


/*!
  \fn void swapCroix(cubie Cube[TAILLE_CUBE], matrices m_Matrices)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sun Jan 26 17:00:04 2020
  \brief Permet de rearranger correctement la croix
  \param Cube cube utilisé
  \param m_Matrices matrices de mouvement

  A partir d'une croix partiellement triée, la procédure inverses les deux cubes mal placées s'ils le sont, puis pivote la face blanche pour aligner correctement la croix
*/
void swapCroix(cubie Cube[TAILLE_CUBE], Matrices m_Matrices);

/*!
  \fn void inversionCroix(cubie Cube[TAILLE_CUBE], matrices m_Matrices, triple t_pos)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sun Jan 26 18:52:02 2020
  \brief permet d'inverser l'arrete t_pos et son opposé pour permuter correctement la croix
  \param Cube cube a utiliser
  \param m_Matrices matrices de mouvement
  \param t_pos position d'une des aretes e inverser
*/
void inversionCroix(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, triple t_pos);

/*!
  \fn int estSuivante(triple t_pos1, triple t_pos2, int int_sens)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sun Jan 26 17:11:47 2020
  \brief Verifie si la postition d'arrivee de l'arete est correcte pour un mouv de type U / D
  \param t_pos1 position de depart
  \param t_pos2 position d'arrivee
  \param int_sens sens de rotation
  \return 1 si le cubie en pos1 se retrouve en pos2, 0 sinon

  En cas de rotation U ou D, renvoi vrai si la position2 sera la postion de l'arete actuellement en position1 apres le mouvement :  (U / D') pour le sens 1 et (U' / D) pour le sens -1
*/
int estSuivante(triple t_pos1, triple t_pos2, int int_sens);


/*!
  \fn int indexActuel(cubie Cube[TAILLE_CUBE], triple t_pos)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sun Jan 26 17:51:29 2020
  \brief Renvoit l'indice dans le Cube du cubie de position actuelle t_pos
  \param Cube cube a utiliser
  \param t_pos Position actuelle du cubie
  \return indice du cubie dans le dube
*/
int indexActuel(cubie Cube[TAILLE_CUBE], triple t_pos);

/*!
  \fn int indexFinal(cubie Cube[TAILLE_CUBE], triple t_pos)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sun Jan 26 17:51:29 2020
  \brief Renvoit l'indice dans le Cube du cubie de position finale t_pos
  \param Cube cube a utiliser
  \param t_pos Position finale du cubie
  \return indice du cubie dans le cube
*/
int indexFinal(cubie Cube[TAILLE_CUBE], triple t_pos);



/*!
  \fn void retourneAreteCroix(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Wed Jan 29 14:26:44 2020
  \brief permet de retourner une arete de la croix
  \param Cube cube utilise
  \param m_Matrices matrices de mouvements
  \param int_index index de l'arete dans le cube
*/
void retourneAreteCroix(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index);

/*!
  \fn void moveAreteCouronne(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, int int_dest)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Wed Jan 29 15:20:49 2020
  \brief Fait descendre une arete de la couronne vers la croix
  \param Cube cube utilise
  \param m_Matrices matrices de mouvements
  \param int_index index de l'arete dans le cube
  \param int_dest destination de l'arete sur la face blanche
*/
void moveAreteCouronne(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, int int_dest);

/*!
  \fn void replaceAreteLateral(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, triple t_face)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Thu Jan 30 14:18:20 2020
  \brief Replace une arete au niveau d'une face latérale donnée
  \param Cube cube utilise
  \param m_Matrices matrices de mouvements
  \param int_index index de l'arete dans le cube
  \param t_face face latérale de référence

  Replace une arete de la face blanche ou jaune au meme niveau qu'une des faces latérales en faisant une rotation de la face blanche ou jaune

*/
void replaceAreteLateral(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, triple t_face);


/*!
  \fn void moveAreteHaut(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, int int_dest)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Thu Jan 30 16:49:02 2020
  \brief Fait descendre une arete de la face du haut vers la croix
  \param Cube cube utilise
  \param m_Matrices matrices de mouvements
  \param int_index index de l'arete dans le cube
  \param int_dest destination de l'arete sur la face blanche
*/
void moveAreteHaut(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, int int_dest);


/*!
  \fn void faireCroix(cubie Cube[TAILLE_CUBE], Matrices m_Matrices)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Thu Jan 30 18:06:47 2020
  \brief permet de placer les elements de la croix opposes deux a deux
  \param Cube cube utilisé
  \param m_Matrices matrices de mouvement
*/
void faireCroix(cubie Cube[TAILLE_CUBE], Matrices m_Matrices);


/*!
  \fn int selectDestCroix(cubie Cube[TAILLE_CUBE])
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sat Feb  1 15:47:15 2020
  \brief Selectionne la case où placer une arete pour la croix
  \param Cube cube utilisé
  \return indice du cubie si l'un d'eux est disponible, -1 sinon

  Selectionne la case où placer une arete pour la croix. Si aucune n'est disponible, la fonction renvoie -1
*/
int selectDestCroix(cubie Cube[TAILLE_CUBE]);



#endif
