/*!
  \file main.c
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Mon Dec  9 20:51:20 2019
  \brief fichier principal

*/

//#include <stdio.h>
//#include <stdlib.h>
#include "structures.h"
#include "mouvement.h"
#include "patron.h"
#include "resolutionCroix.h"
#include "resolutionCouronne.h"

/*!
  \fn int main (void)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Mon Dec  9 20:51:20 2019
  \brief fonction principale
  \return 0 si tout va bien

*/

int main (void) {
  /* initialisation des variables */
  // triple position;
  int i, j, k, l;

  /* Matrices de mouvement */
  Matrices m_Matrices = {
    {
    {1, 0, 0},
    {0, 0, 1},                  /* Matrice R / L' */
    {0, -1, 0}},
    {
    {1, 0, 0},
    {0, 0, -1},                 /* Matrice R' / L */
    {0, 1, 0}},
    {
    {0, 0, -1},
    {0, 1, 0},                  /* Matrice U / D' */
    {1, 0, 0}},
    {
    {0, 0, 1},
    {0, 1, 0},                  /* Matrice U' / D */
    {-1, 0, 0}},
    {
    {0, 1, 0},
    {-1, 0, 0},                 /* Matrice F / B' */
    {0, 0, 1}},
    {
    {0, -1, 0},
    {1, 0, 0},                  /* Matrice F' / B */
    {0, 0, 1}}};


  //Declaration cube Initial
  l = 0;
  cubie mainCube[TAILLE_CUBE];
  for (i = -1 ; i < 2 ; i++) {
    for (j = -1 ; j < 2 ; j++) {
      for (k = -1 ; k < 2 ; k++) {
        if (!(((i == 0)&&(j == 0)) || ((i == 0)&&(k == 0)) || ((j == 0)&&(k == 0)))) {
          (mainCube[l]).posActuelle = initTriple(i, j, k);
          (mainCube[l]).posFinale = initTriple(i, j, k);

          (mainCube[l]).int_orient = 0;
          (mainCube[l]).couleur = initTriple(0, 0, 0);
          l++;
        }
      }
    }
  }
  //

  //attribution couleurs
  triple couleursResolu[TAILLE_CUBE] = {initTriple(5, 1, 4), initTriple(0, 1, 5), initTriple(3, 1, 5),
                                        initTriple(0, 4, 5), initTriple(0, 3, 5), initTriple(4, 2, 5),
                                        initTriple(0, 2, 5), initTriple(5, 2, 3), initTriple(0, 1, 4),
                                        initTriple(0, 1, 3), initTriple(0, 2, 4), initTriple(0, 2, 3),
                                        initTriple(4, 1, 6), initTriple(0, 1, 6), initTriple(6, 1, 3),
                                        initTriple(0, 4, 6), initTriple(0, 3, 6), initTriple(6, 2, 4),
                                        initTriple(0, 2, 6), initTriple(3, 2, 6)};
  //Declaration cube resolu
  l = 0;
  cubie cubeResolu[TAILLE_CUBE];
  for (i = -1 ; i < 2 ; i++) {
    for (j = -1 ; j < 2 ; j++) {
      for (k = -1 ; k < 2 ; k++) {
        if (!(((i == 0)&&(j == 0)) || ((i == 0)&&(k == 0)) || ((j == 0)&&(k == 0)))) {
          (cubeResolu[l]).posActuelle = initTriple(i, j, k);
          (cubeResolu[l]).posFinale = initTriple(i, j, k);

          (cubeResolu[l]).int_orient = 0;
          (cubeResolu[l]).couleur = couleursResolu[l];
          l++;
        }
      }
    }
  }





  //Tableau correspondance cube <--> patron
  int t_orientations[48] = {0, 0, 0, 0, 0, 0, 0, 0,  //J
                                     1, 1, -1, 1, 1, -1, 1, 1, //O
                                     1, 1, -1, 0, 0, -1, 1, 1,  //B
                                     1, 1, -1, 1, 1, -1, 1, 1,  //R
                                     0, 0, 0, 0, 0, 0, 0, 0,  //W
                                     1, 1, -1, 0, 0, -1, 1, 1}; //V

    int t_positions[48] = {6, 11, 18, 7, 19, 8, 12, 20,  //J
                                    6, 7, 8, 4, 5, 1, 2, 3,  //O
                                    8, 12, 20, 5, 17, 3, 10, 15,  //B
                                    20, 19, 18, 17, 16, 15, 14, 13,  //R
                                    3, 10, 15, 2, 14, 1, 9, 13,  //W
                                    1, 9, 13, 4, 16, 6, 11, 18};  //V

    ///////TESTS SAISIES COULEURS

    demandeSaisieCube(mainCube, cubeResolu, t_orientations, t_positions);

    /// TEST RESOLUTION CROIX
    faireCroix(mainCube, m_Matrices);
    swapCroix(mainCube, m_Matrices);
    coinsFaceBlanche(mainCube, m_Matrices);    


  // affichage rapide de l'etat du cube
  for (i = 0 ; i < TAILLE_CUBE ; i++) {
    printf("(%d, %d, %d), (%d, %d, %d) , orient : %d\n", (mainCube[i]).posActuelle[0], (mainCube[i]).posActuelle[1], (mainCube[i]).posActuelle[2], (mainCube[i]).posFinale[0], (mainCube[i]).posFinale[1], (mainCube[i]).posFinale[2], (mainCube[i]).int_orient);
  }


  /*for (i = 0 ; i < TAILLE_CUBE ; i++) {
    printf("(%d, %d, %d), (%d, %d, %d) , orient : %d\n", (cubeResolu[i]).posActuelle[0], (cubeResolu[i]).posActuelle[1], (cubeResolu[i]).posActuelle[2], (cubeResolu[i]).posFinale[0], (cubeResolu[i]).posFinale[1], (cubeResolu[i]).posFinale[2], (cubeResolu[i]).int_orient);
    }*/

  printf("\n\n");

  //libere la memoire

  for (i = 0 ; i < TAILLE_CUBE ; i++) {
    free((mainCube[i]).posActuelle);
    free((mainCube[i]).posFinale);
    free((mainCube[i]).couleur);
    free((cubeResolu[i]).posActuelle);
    free((cubeResolu[i]).posFinale);
    free((cubeResolu[i]).couleur);
  }

  /* fin du programme */
  return 0;
}
