/*!
*  \file resolutionCouronne.c
*  \author Aurélien Carmes <carmesaure@eisti.eu>
*  \version 0.1
*  \date  2020-02-10
*  \brief fichier declaration fonctions
*  \remarks Resolution du deuxieme etage du cube
*/

#include "resolutionCouronne.h"

/*
 * Remonte un coin de la face blanche ver la face du haut, à la destination spéfiée
*/
void remonteCoin(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, int int_dest)
{
  triple t_faceA, t_faceC;
  triple fJaune;
  fJaune = initTriple(0, 1, 0);

  /* Selectionne les faces de devant et de coté */
  if (Cube[int_index].posActuelle[0] * Cube[int_index].posActuelle[2] == 1) {
    t_faceA = initTriple(0, 0, Cube[int_index].posActuelle[2]);
    t_faceC = initTriple(Cube[int_index].posActuelle[0], 0, 0);
  }
  else {
    t_faceA = initTriple(Cube[int_index].posActuelle[0], 0, 0);
    t_faceC = initTriple(0, 0, Cube[int_index].posActuelle[2]);
  }

  /* Remonte le coin selon les 4 positions possibles de la destination */
  if ((Cube[int_dest].posActuelle[0] == Cube[int_index].posActuelle[0]) && (Cube[int_dest].posActuelle[2] == Cube[int_index].posActuelle[2])) { /* Si la dest est au dessus */
    mouvFace(Cube, m_Matrices, t_faceA, -1);
    mouvFace(Cube, m_Matrices, fJaune, -1);
    mouvFace(Cube, m_Matrices, t_faceA, 1);
    mouvFace(Cube, m_Matrices, fJaune, 1);
  }
  else if ((Cube[int_dest].posActuelle[0] == t_faceA[0]) || (Cube[int_dest].posActuelle[2] == t_faceA[2])) { /* Si la dest est sur la face de devant */
    mouvFace(Cube, m_Matrices, t_faceC, 1);
    mouvFace(Cube, m_Matrices, fJaune, 1);
    mouvFace(Cube, m_Matrices, t_faceC, -1);
  }
  else if ((Cube[int_dest].posActuelle[0] != Cube[int_index].posActuelle[0]) && (Cube[int_dest].posActuelle[2] != Cube[int_index].posActuelle[2])) { /* Si la dest est sur la face de derriere */
    mouvFace(Cube, m_Matrices, t_faceA, -1);
    mouvFace(Cube, m_Matrices, fJaune, -1);
    mouvFace(Cube, m_Matrices, fJaune, -1);
    mouvFace(Cube, m_Matrices, t_faceA, 1);
  }
  else { /* Si la dest est sur la face de coté */
    mouvFace(Cube, m_Matrices, t_faceA, -1);
    mouvFace(Cube, m_Matrices, fJaune, -1);
    mouvFace(Cube, m_Matrices, t_faceA, 1);
  }

  free(fJaune);
  free(t_faceA);
  free(t_faceC);
}


/*
 * Replace un coin de la face jaune vers la position en dessus sur la face blanche
 */
void replaceCoin(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index)
{
  triple t_faceA, t_faceC;
  triple fJaune;
  fJaune = initTriple(0, 1, 0);

  /* Selectionne les faces de devant et de coté */
  if (Cube[int_index].posActuelle[0] * Cube[int_index].posActuelle[2] == 1) {
    t_faceA = initTriple(0, 0, Cube[int_index].posActuelle[2]);
    t_faceC = initTriple(Cube[int_index].posActuelle[0], 0, 0);
  }
  else {
    t_faceA = initTriple(Cube[int_index].posActuelle[0], 0, 0);
    t_faceC = initTriple(0, 0, Cube[int_index].posActuelle[2]);
  }

  /* Replace le coin selon les 3 orientations possibles */
  if (Cube[int_index].int_orient == 0) {
    mouvFace(Cube, m_Matrices, t_faceC, 1);
    mouvFace(Cube, m_Matrices, fJaune, 1);
    mouvFace(Cube, m_Matrices, fJaune, 1);
    mouvFace(Cube, m_Matrices, t_faceC, -1);
    mouvFace(Cube, m_Matrices, fJaune, -1);
    mouvFace(Cube, m_Matrices, t_faceC, 1);
    mouvFace(Cube, m_Matrices, fJaune, 1);
    mouvFace(Cube, m_Matrices, t_faceC, -1);
  }  
  else if (Cube[int_index].int_orient == 1) {
    mouvFace(Cube, m_Matrices, t_faceC, 1);
    mouvFace(Cube, m_Matrices, fJaune, 1);
    mouvFace(Cube, m_Matrices, t_faceC, -1);
  }
  else {
    mouvFace(Cube, m_Matrices, fJaune, 1);
    mouvFace(Cube, m_Matrices, t_faceC, 1);
    mouvFace(Cube, m_Matrices, fJaune, -1);
    mouvFace(Cube, m_Matrices, t_faceC, -1);
  }

  free(fJaune);
  free(t_faceA);
  free(t_faceC);
}


// Replace tous les coins de la face Blanche, finit la premiere couronne
void coinsFaceBlanche(cubie Cube[TAILLE_CUBE], Matrices m_Matrices)
{
  int int_dest, i;
  int int_coins[4];
  triple t_faceA, t_faceC;
  triple fJaune;
  fJaune = initTriple(0, 1, 0);

  int_coins[0] = indexFinal(Cube, initTriple(-1, -1, -1));
  int_coins[1] = indexFinal(Cube, initTriple(1, -1, -1));
  int_coins[2] = indexFinal(Cube, initTriple(-1, -1, 1));
  int_coins[3] = indexFinal(Cube, initTriple(1, -1, 1));

  for (i = 0; i < 4; i++) {
    /* Selectionne les faces de devant et de coté */
    if (Cube[int_coins[i]].posActuelle[0] * Cube[int_coins[i]].posActuelle[2] == 1) {
      t_faceA = initTriple(0, 0, Cube[int_coins[i]].posActuelle[2]);
      t_faceC = initTriple(Cube[int_coins[i]].posActuelle[0], 0, 0);
    }
    else {
      t_faceA = initTriple(Cube[int_coins[i]].posActuelle[0], 0, 0);
      t_faceC = initTriple(0, 0, Cube[int_coins[i]].posActuelle[2]);
    }

    int_dest = indexActuel(Cube, initTriple(Cube[int_coins[i]].posFinale[0], 1, Cube[int_coins[i]].posFinale[2]));

    /*  Replace le coin au dessus de sa destination si nécessaire  */
    if ((Cube[int_coins[i]].posActuelle[1] == -1) && (!(compareTriple(Cube[int_coins[i]].posActuelle, Cube[int_coins[i]].posFinale) && (Cube[int_coins[i]].int_orient == 0)))) { /* Remonte le coin s'il est mal placé sur la face blanche  */
      remonteCoin(Cube, m_Matrices, int_coins[i], int_dest);
    }     
    else if ((Cube[int_dest].posActuelle[0] != Cube[int_coins[i]].posActuelle[0]) && (Cube[int_dest].posActuelle[2] != Cube[int_coins[i]].posActuelle[2])) {  /* Si la dest est derriere */
      mouvFace(Cube, m_Matrices, fJaune, 1);
      mouvFace(Cube, m_Matrices, fJaune, 1);
    }    
    else if ((Cube[int_dest].posActuelle[0] == t_faceA[0]) || (Cube[int_dest].posActuelle[2] == t_faceA[2])) { /* Si la dest est sur la face de devant */
      mouvFace(Cube, m_Matrices, fJaune, 1);
    }
    else if ((Cube[int_dest].posActuelle[0] == t_faceC[0]) || (Cube[int_dest].posActuelle[2] == t_faceC[2])) { /* Si la dest est sur la face de coté */
      mouvFace(Cube, m_Matrices, fJaune, -1);
    }

    replaceCoin(Cube, m_Matrices, int_coins[i]); /* Replace le coin */

    free(t_faceA);
    free(t_faceC);
  }

  free(fJaune);
}
