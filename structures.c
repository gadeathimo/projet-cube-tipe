/*!
  \file structures.c
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Wed Dec 11 17:05:35 2019
  \brief fichier principale
  \remarks definition des fonctions associées aux structures et types
*/

#include "structures.h"

////////////////////////////FONCTIONS TRIPLE
/* Initialise un nouveau triplé, avec (0, 0, 0) par défaut */
triple initTriple(int x, int y, int z )
{
  triple pint_retour;
  pint_retour = malloc(3 * sizeof(int));
  pint_retour[0] = x;
  pint_retour[1] = y;
  pint_retour[2] = z;

  return pint_retour;
}

/*
copie les valeurs du vecteur passé en parametre
*/
void copyTriple(triple vec0, triple vec1)
{
  int i;
  for (i = 0 ; i < N ; i++) {
    vec0[i] = vec1[i];
  }
}


////////////////////////////

/*
Renvoie si le cubie est une arete ou un coin
*/
char typeCubie(triple vec_position)
{
  int i;
  char char_retour;

  char_retour = 'c';

  for (i = 0 ; i < N ; i++) {
    if (vec_position[i] == 0) {
      char_retour = 'a';
    }
  }
  return char_retour;
}

/*
renvoi 1 si les 2 triples contiennent les meme valeur, a l'ordre pres, 0 sinon
*/
int compareTriple (triple t_pos1, triple t_pos2)
{

  return ((t_pos1[0] == t_pos2[0]) && (t_pos1[1] == t_pos2[1]) && (t_pos1[2] == t_pos2[2]));

}


/*
  Retourne l'indice de la premiere coordonnee possedant la valeur specifiee ; Si la valeur n'est pas presente, le retour vaut -1
*/
int indexValeur(triple vec, int int_val)
{
  int int_retour;

  int_retour = -1;

  if (vec[0] == int_val) {
    int_retour = 0;
  }
  else if (vec[1] == int_val) {
    int_retour = 1;
  }
  else if (vec[2] == int_val) {
    int_retour = 2;
  }

  return int_retour;
}



/*
Fait la somme de deux valeurs d'orientation (pour un coin) de manière cyclique : 1 + 1 donne (-1)    et    (-1) + (-1) donne 1 ,  l'orientation vaut donc toujours -1, 0 ou 1
*/
int addOrient(int int_orient1, int int_orient2)
{
  int int_val;

  int_val = int_orient1 + int_orient2;
  if ((int_val == -2) || (int_val == 2)) {
    int_val /= (-2);
  }

  return int_val;
}
