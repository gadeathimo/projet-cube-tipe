/*!
*  \file resolutionCouronne.h
*  \author Aurélien Carmes <carmesaure@eisti.eu>
*  \version 0.1
*  \date  2020-02-10
*  \brief fichier en-tete
*  \remarks Resolution du deuxieme etage du cube
*/ 

#ifndef RESOLUTIONCOURONNE_H
#define RESOLUTIONCOURONNE_H

#include "mouvement.h"
#include "structures.h"
#include "resolutionCroix.h"

/*!
*  \fn void remonteCoin(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, int int_dest)
*  \author Aurélien Carmes <carmesaure@eisti.eu>
*  \version 0.1
*  \date  2020-02-10
*  \brief Remonte un coin de la face blanche ver la face du haut, à la destination spéfiée
*  \param Cube cube utilise
*  \param m_Matrices matrices de mouvement
*  \param int_index index du coin à remonter
*  \param int_dest index de la destination sur la face jaune
*/ 
void remonteCoin(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index, int int_dest);

/*!
*  \fn void replaceCoin(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index)
*  \author Aurélien Carmes <carmesaure@eisti.eu>
*  \version 0.1
*  \date  2020-02-10
*  \brief Replace un coin de la face jaune vers la position en dessus sur la face blanche
*  \param Cube cube utilise
*  \param m_Matrices matrices de mouvement
*  \param int_index index du coin à remonter
*/ 
void replaceCoin(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, int int_index);

/*!
*  \fn void coinsFaceBlanche(cubie Cube[TAILLE_CUBE], Matrices m_Matrices)
*  \author Aurélien Carmes <carmesaure@eisti.eu>
*  \version 0.1
*  \date  2020-02-10
*  \brief Replace tous les coins de la face Blanche, finit la premiere couronne
*  \param Cube cube utilise
*  \param m_Matrices matrices de mouvement
*/ 
void coinsFaceBlanche(cubie Cube[TAILLE_CUBE], Matrices m_Matrices);


#endif
