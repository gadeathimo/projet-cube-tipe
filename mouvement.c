/*!
  \file mouvement.c
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Mon Dec  9 20:18:55 2019
  \brief fichier principal
  \remarks mouvements et affichage de base
*/


#include "mouvement.h"

/*!
  \def ERREUR_MOUV
  erreur concernant l'application d'un mouvement
*/
#define ERREUR_MOUV -10



/* Effectue le produit matriciel d'un vecteur avec une matrice carree de taille 3 */
triple matProd (triple vec, int tt_mat[N][N])
{
  int i, j;
  triple vec_retour;

  vec_retour = initTriple(0, 0, 0);

  for (i = 0 ; i < N ; i++) {
    for (j = 0 ; j < N ; j++) {
      vec_retour[i] += vec[j] * tt_mat[i][j];
    }
  }

  free(vec);
  return vec_retour;
}


/*
  La procédure applique la matrice de mouvement à tous les cubies de la face concernée, et effectue les changements d'orientation. Le paramètre triple face est les coordonnées du centre de la face.
*/
void appliquerMouv (cubie Cube[TAILLE_CUBE], int tt_mat[N][N], triple triple_face)
{
  int i;
  int int_face;

  /* determine la coordonnee constante de la face */
  for (i = 0 ; i < N ; i++) {
    if (triple_face[i] != 0) {
      int_face = i;
    }
  }

  for (i = 0 ; i < TAILLE_CUBE ; i++) {

    /* effectue le changement de coordonnees pour les cubie de la face concernee*/
    if (((Cube[i]).posActuelle[int_face]) == triple_face[int_face]) {
      (Cube[i]).posActuelle = matProd((Cube[i]).posActuelle, tt_mat);
      appliquerOrient(&Cube[i], int_face);
    }
  }
}


/*
applique le changement d'orientation asocié a un mouvement
*/
void appliquerOrient (cubie* c_cubie, int int_indConst)
{
  int int_modifCoord;  // changement à effectuer (pour les coins)

  int_modifCoord = 0;

  if (typeCubie((*c_cubie).posActuelle) == 'c') {    /* coins */
    if (int_indConst == 0) {    /* cas L/R */
      int_modifCoord = ((*c_cubie).posActuelle[0]) * ((*c_cubie).posActuelle[1]) * ((*c_cubie).posActuelle[2]) * (-1);
    }
    else if (int_indConst == 2) {    /* cas F/B */
      int_modifCoord = ((*c_cubie).posActuelle[0]) * ((*c_cubie).posActuelle[1]) * ((*c_cubie).posActuelle[2]);
    }

    (*c_cubie).int_orient = addOrient((*c_cubie).int_orient, int_modifCoord);
  }
  else if (int_indConst == 2) {     /* aretes */
    /* inversion de la valeur d'orientation */
    (*c_cubie).int_orient = !((*c_cubie).int_orient);
  }
}



/*
Effectue une rotation de la face de centre triple_face dans le sens int_sens (1 pour sens horaire, -1 sens anti-horaire).
*/
void mouvFace(cubie Cube[TAILLE_CUBE], Matrices m_Matrices, triple triple_face, int int_sens)
{
  if (((triple_face[0] == 1) && (int_sens == 1)) || ((triple_face[0] == -1) && (int_sens == -1))) { /* Mouvement R / L' */
    appliquerMouv(Cube, m_Matrices.matriceRL, triple_face);
    if (triple_face[0] == 1) {
      printf("R\n");
    }
    else {
      printf("L'\n");
    }
  }
  else if (((triple_face[0] == 1) && (int_sens == -1)) || ((triple_face[0] == -1) && (int_sens == 1))) { /* Mouvement R' / L */
    appliquerMouv(Cube, m_Matrices.matriceInvRL, triple_face);
    if (triple_face[0] == 1) {
      printf("R'\n");
    }
    else {
      printf("L\n");
    }
  }
  else if (((triple_face[1] == 1) && (int_sens == 1)) || ((triple_face[1] == -1) && (int_sens == -1))) { /* Mouvement U / D' */
    appliquerMouv(Cube, m_Matrices.matriceUD, triple_face);
    if (triple_face[1] == 1) {
      printf("U\n");
    }
    else {
      printf("D'\n");
    }
  }
  else if (((triple_face[1] == 1) && (int_sens == -1)) || ((triple_face[1] == -1) && (int_sens == 1))) { /* Mouvement U' / D */
    appliquerMouv(Cube, m_Matrices.matriceInvUD, triple_face);
    if (triple_face[1] == 1) {
      printf("U'\n");
    }
    else {
      printf("D\n");
    }
  }
  else if (((triple_face[2] == 1) && (int_sens == 1)) || ((triple_face[2] == -1) && (int_sens == -1))) { /* Mouvement F / B' */
    appliquerMouv(Cube, m_Matrices.matriceFB, triple_face);
    if (triple_face[2] == 1) {
      printf("F\n");
    }
    else {
      printf("B'\n");
    }
  }
  else if (((triple_face[2] == 1) && (int_sens == -1)) || ((triple_face[2] == -1) && (int_sens == 1))) { /* Mouvement F' / B */
    appliquerMouv(Cube, m_Matrices.matriceInvFB, triple_face);
    if (triple_face[2] == 1) {
      printf("F'\n");
    }
    else {
      printf("B\n");
    }
  }
  else {                        /* En cas d'erreur */
    fprintf(stderr, "Erreur saisie mouvement\n");
    exit(ERREUR_MOUV);
  }
}
