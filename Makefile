Compilateur=gcc
FLAGS=-Wall
FILES=main.c patron.c mouvement.c structures.c resolutionCroix.c resolutionCouronne.c

all:
	@$(Compilateur) $(FLAGS) $(FILES) -o progtest
	@echo "[+]Compilation Terminee[+]"
