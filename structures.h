/*!
  \file structures.h
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Mon Dec  9 19:46:02 2019
  \brief fichier de deckarations des structures et enum

  Contient les principales structures et enumerations pour definir le cube
*/

#ifndef STRUCTURES_H
#define STRUCTURES_H


#include <stdio.h>
#include <stdlib.h>

/*!
  \def N
  taille vecteur et matrice
*/
#define N 3
/*!
  \def TAILLE_CUBE
  nombre de cubies à manipuler dans le cube
*/
#define TAILLE_CUBE 20


typedef int* triple;

/*! \struct cubie
 * Structure d'un coin ou d'une arête
 * Orient est 0 ou 1 pour une arete et -1, 0 ou 1 pour un coin
 */
typedef struct cubie {
  int int_orient;             /*!< Valeur d'orientation */
  triple posFinale;           /*!< Position dans le cube résolu */
  triple posActuelle;         /*!< Position actuelle */
  triple couleur;             /*!< Couleurs de chaque facette */
} cubie ;


/*! \struct Matrices
  strcture contenant les matrices de mouvement
*/
typedef struct Matrices {
  int matriceRL[N][N];          /*!< Matrice R / L' */
  int matriceInvRL[N][N];       /*!< Matrice R' / L */
  int matriceUD[N][N];          /*!< Matrice U / D' */
  int matriceInvUD[N][N];       /*!< Matrice U' / D */
  int matriceFB[N][N];          /*!< Matrice F / B' */
  int matriceInvFB[N][N];       /*!< Matrice F' / B */
} Matrices ;


//////////////////////Fonctions manipulation type triple
/*!
  \fn triple initTriple(int x = 0, int y = 0, int z = 0)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Wed Dec 11 16:59:36 2019
  \brief Initialise un nouveau triplé, avec (0, 0, 0) par défaut
  \param x coordonnee 0
  \param y coordonnee 1
  \param z coordonnee 2
  \return triple initialisé
*/
triple initTriple(int x, int y, int z);


/*!
  \fn void copyTriple(triple vec)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sun Dec 15 13:28:06 2019
  \brief copie les valeurs du vecteur passé en parametre
  \param vec0 vecteur receveur
  \param vec1 vecteur à copier
*/
void copyTriple(triple vec0, triple vec1);


///////////////////////////////




/*!
  \fn char typeCubie(triple vec_position)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sun Dec 15 12:45:00 2019
  \brief Renvoie si le cubie est une arete ou un coin
  \param vec_position coordonnees du cubie
  \return 'c' si c'est un coin, 'a' si c'est une arete
*/
char typeCubie(triple vec_position);

/*!
  \fn int compareTriple (triple t_pos1, triple t_pos2)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Tue Jan  7 18:18:32 2020
  \brief renvoi 1 si les 2 triples contiennent les meme valeur, a l'ordre pres, 0 sinon
  \param t_pos1 premier triple a comparer
  \param t_pos2 deuxieme triple a comparer
  \return booleen, 1 ou 0 selon les contenus des triples
*/
int compareTriple (triple t_pos1, triple t_pos2);


/*!
  \fn int indexValeur(triple vec, int int_val)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sun Dec 15 12:59:47 2019
  \brief retourne l'indice de la coordonnee possedant une certaine valeur
  \param vec triple utilisé
  \param int_val valeur a rechercher
  \retour entier, index de la valeur dans le triple ; -1 si elle n'existe pas

  Retourne l'indice de la premiere coordonnee possedant la valeur specifiee ; Si la valeur n'est pas presente, le retour vaut -1
*/
int indexValeur(triple vec, int int_val);


/*!
  \fn int addOrient(int int_orient1, int int_orient2)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Sun Dec 15 17:39:35 2019
  \brief Ajoute deux valeurs d'orientation de maniere cyclique
  \param int_orient1 valeur d'orientation 1
  \param int_orient2 valeur d'orientation 2
  \return nouvelle valeur d'orientation

  Fait la somme de deux valeurs d'orientation (pour un coin) de manière cyclique : 1 + 1 donne (-1)    et    (-1) + (-1) donne 1 ,  l'orientation vaut donc toujours -1, 0 ou 1
*/
int addOrient(int int_orient1, int int_orient2);



/*!
  \fn triple initTriple(int x = 0, int y = 0, int z = 0)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Wed Dec 11 16:59:36 2019
  \brief Initialise un nouveau triplé, avec (0, 0, 0) par défaut
  \param x coordonnee 0
  \param y coordonnee 1
  \param z coordonnee 2
  \return triple initialisé
*/
triple initTriple(int x, int y, int z);




#endif
