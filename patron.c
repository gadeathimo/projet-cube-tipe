#include <stdlib.h>
#include <stdio.h>

#include "patron.h"

/*
\fn void affichagepatron()
\brief
\author Hugo POINTEREAU <pointereau@eisti.eu>
\version 0.1
\date
*/
void affichagepatron (){
  for (size_t i = 0; i < 12; i++) {
    for (size_t j = 0; j < 9; j++) {
      if (((j<3) ||(j>5)) &&((i<3)||(i>5))) {
        printf("   ");
      }
      if (((j>=3)&&(j<6))&&((i<3)||(i>5))) {
        printf(" x ");
      }
      if (i>=3 && i<6) {
        printf(" x ");
      }
    }
    printf("\n");
  }
}


/*
Enregistre l'information de couleur saisie par l'utilisateur
*/
void saisieCouleur(cubie Cube[TAILLE_CUBE], int int_index, int int_couleur, int t_orientations[48], int t_positions[48])
{
  (Cube[t_positions[int_index] - 1]).couleur[t_orientations[int_index] + 1] = int_couleur;
}


/*
Remplit les infos manquantes dans le cube apres la saisie des couleurs
*/
void remplissageStatut(cubie Cube[TAILLE_CUBE], cubie CubeResolu[TAILLE_CUBE])
{
  int i, j;

  for (i = 0 ; i < TAILLE_CUBE ; i++) {
    j = 0;
    while (!(compareCouleurs(Cube[i].couleur, CubeResolu[j].couleur)))
    {
      j++;
    }
    copyTriple((Cube[i]).posFinale, CubeResolu[j].posFinale);

    Cube[i].int_orient = 0;
    if ((Cube[i]).couleur[0] == 0) {
      if (Cube[i].couleur[1] == CubeResolu[j].couleur[2]) {
        Cube[i].int_orient = 1;
      }
    }
    else
    {
      if (Cube[i].couleur[1] == CubeResolu[j].couleur[0]) {
        Cube[i].int_orient = 1;
      }
      else if (Cube[i].couleur[1] == CubeResolu[j].couleur[2]) {
        Cube[i].int_orient = -1;
      }
    }
  }
}

/*
renvoi 1 si les 2 triples contiennent les meme valeur, a l'ordre pres, 0 sinon
*/
int compareCouleurs (triple t_couleurs1, triple t_couleurs2)
{
  int int_retour, int_retourTemp;
  int i;
  triple t_temp;

  t_temp = initTriple(0, 0, 0);
  int_retour = 1;

  for (i = 0 ; i < 3 ; i++) {
    int_retourTemp = 1;
    if ((t_couleurs1[i] == t_couleurs2[0]) && !(t_temp[0])) {
      t_temp[0] = 1;
      int_retourTemp = 0;;
    }
    else if ((t_couleurs1[i] == t_couleurs2[1]) && !(t_temp[1])) {
      t_temp[1] = 1;
      int_retourTemp = 0;
    }
    else if ((t_couleurs1[i] == t_couleurs2[2]) && !(t_temp[2])) {
      t_temp[2] = 1;
      int_retourTemp = 0;
    }

    if (int_retourTemp) {
      int_retour = 0;
    }
  }

  return int_retour;
}



int  CharToInt(char char_Lettre_Couleur)
{
  int int_retour;

  switch(char_Lettre_Couleur){
  case 'w':
  case 'W':int_retour=1;
  break;
  case 'y':
  case 'Y':int_retour=2;
  break;
  case 'b':
  case 'B':int_retour=3;
  break;
  case 'g':
  case 'G':int_retour=4;
  break;
  case 'o':
  case 'O':int_retour=5;
  break;
  case 'r':
  case 'R':int_retour=6;
  break;
  default:
    printf("Probleme dans la saisie de la lettre \n");
  }
  return(int_retour);
}



void AfficherCouleur(int int_couleur)
{
  switch(int_couleur){
  case 1:printf("| ");printf("\033[0;93m");printf("■ ");printf("\033[0m");
  break;
  case 2:printf("| ");printf("\033[0;33m");printf("■ ");printf("\033[0m");
  break;
  case 3:printf("| ");printf("\033[0;34m");printf("■ ");printf("\033[0m");
  break;
  case 4:printf("| ");printf("\033[0;31m");printf("■ ");printf("\033[0m");
  break;
  case 5:printf("| ");printf("\033[0;37m");printf("■ ");printf("\033[0m");
  break;
  case 6:printf("| ");printf("\033[0;32m");printf("■ ");printf("\033[0m");
  break;
  default:
    printf("Probleme de valeur: %d\n",int_couleur);
  }
}


void affichageCubeCurrentEmplacement(int int_pos_x,int int_pos_y, int int_couleur)
{
  int int_i;
  int int_j;

  printf("+---+---+---+\n");
  for(int_i=0;int_i<3;int_i++){
    //printf("|  ");
    for(int_j=0;int_j<3;int_j++){
        if((int_pos_x==int_i) && (int_pos_y==int_j)&&((int_pos_x!=1)||(int_pos_y!=1))){
        printf("| x ");
        //int_case_couleur=SaisirCubie();
        //AfficherCouleur(int_couleur);
      }
    else if(int_i==1 && int_j==1){
          AfficherCouleur(int_couleur);
              }
           else{
      printf("|   ");
           }
     }
    printf("|\n");
    printf("+---+---+---+\n");
    }
printf("\n");
}



void demandeSaisieCube (cubie Cube[TAILLE_CUBE], cubie CubeResolu[TAILLE_CUBE], int t_orientations[48], int t_positions[48])
{
  int int_i;
  int int_j;
  int int_index;
  int int_boucle;
  char char_choix;
  int c;

  int_index = 0;

 for(int_boucle=1;int_boucle<7;int_boucle++){
   for(int_i=0;int_i<3;int_i++){
    for(int_j=0;int_j<3;int_j++){
      if((int_j!=1)||(int_i!=1)){
        system("clear");
        orientation(int_boucle);
        affichageCubeCurrentEmplacement(int_i,int_j,int_boucle);
        printf("rentrez votre couleur!:\n");
        scanf("%c",&char_choix);

        saisieCouleur(Cube, int_index, CharToInt(char_choix), t_orientations, t_positions);

        while((c=getchar()) != '\n' && c != EOF){}
       int_index++;
      }
   }
 printf("\n");
 }


 }

 remplissageStatut(Cube, CubeResolu);
}




void orientation(int int_up)
{
  switch(int_up){
  case 1 :printf("face au dessus: Vert | face en dessous: Bleu\n");
  break;
  case 2 :printf("face au dessus: Jaune | face en dessous: Blanc\n");
  break;
  case 3 :printf("face au dessus: Jaune | face en dessous: Blanc\n");
  break;
  case 4 :printf("face au dessus: Jaune | face en dessous: Blanc\n");
  break;
  case 5 :printf("face au dessus: Bleu | face en dessous: Vert\n");
  break;
  case 6 :printf("face au dessus: Blanc | face en dessous: Jaune\n");
  break;
  default:
    printf("Erreur quelque part\n");
  }

}
