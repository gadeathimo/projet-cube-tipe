/*!
  \file patron.h
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Tue Jan  7 17:08:31 2020
  \brief fichier entete
  \remarks section affichage et saisie
*/

#ifndef PATRON_H
#define PATRON_H

#include "structures.h"


void affichagepatron ();


/*!
  \fn void saisieCouleurint saisieCouleur(cubie Cube[TAILLE_CUBE], int int_index, int int_couleur, int t_orientations[TAILLE_CUBE], int t_positions[TAILLE_CUBE])
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Tue Jan  7 17:10:37 2020
  \brief Enregistre l'information de couleur saisie par l'utilisateur
  \param Cube cube à utiliser
  \param int_index index de la case saisie
  \param int_couleur valeur de la couleur saisie
  \param t_orientations tableau de correspondance orientations
  \param t_positions tableau de correspondance positions
*/
void saisieCouleur(cubie Cube[TAILLE_CUBE], int int_index, int int_couleur, int t_orientations[48], int t_positions[48]);



/*!
  \fn void remplissageStatut(cubie Cube[TAILLE_CUBE], cubie CubeResolu[TAILLE_CUBE])
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Tue Jan  7 17:39:32 2020
  \brief Remplit les infos manquantes dans le cube apres la saisie des couleurs
  \param Cube cube à utiliser
  \param CubeResolu cube resolu (pour comparer)
*/
void remplissageStatut(cubie Cube[TAILLE_CUBE], cubie CubeResolu[TAILLE_CUBE]);

/*!
  \fn int compareCouleurs (triple t_couleurs1, triple t_couleurs2)
  \author Aurélien Carmes <carmesaure@eisti.eu>
  \version 0.1
  \date Tue Jan  7 18:18:32 2020
  \brief renvoi 1 si les 2 triples contiennent les meme valeur, a l'ordre pres, 0 sinon
  \param t_couleurs1 premier triple a comparer
  \param t_couleurs2 deuxieme triple a comparer
  \return booleen, 1 ou 0 selon les contenus des triples
*/
int compareCouleurs (triple t_couleurs1, triple t_couleurs2);


int  CharToInt(char char_Lettre_Couleur);

void AfficherCouleur(int int_couleur);

void affichageCubeCurrentEmplacement(int int_pos_x,int int_pos_y, int int_couleur);

void orientation(int int_up);


/*!
  \fn void demandeSaisieCube (cubie Cube[TAILLE_CUBE], cubie CubeResolu[TAILLE_CUBE], int t_orientations[48], int t_positions[48])
  \version 0.1
  \date Tue Jan 14 17:00:32 2020
  \brief demande la saisie des différentes facettes des cubies
  \param Cube cube à utiliser
  \param CubeResolu cube de référence résolu
  \param t_orientations tableau de correspondance orientations
  \param t_positions tableau de correspondance positions
*/
void demandeSaisieCube (cubie Cube[TAILLE_CUBE], cubie CubeResolu[TAILLE_CUBE], int t_orientations[48], int t_positions[48]);


#endif
